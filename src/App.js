import React from 'react';
import './App.css';
import CustomerLogo from './Header/CustomerLogo';

require('./Styles/_styles.scss');

const App = (props) => {
    return (
      <div className="App">
        <CustomerLogo />
        {props.children}
      </div>
    );
}

export default App;
