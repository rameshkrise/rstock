export const MathUtil = {
    toFixedPrecision: (value, precision) => (
        parseFloat(Math.round(value * 100) / 100).toFixed(precision)
    )
}