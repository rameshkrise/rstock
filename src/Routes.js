import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import App from './App';
import DetailedView from './View/DetailedView';
import HomePage from './View/HomePage';

const Routes = () => (
    <Router>
        <App>
            <Route exact path="/" component={HomePage} />
            <Route path="/details" component={DetailedView} />
        </App>
    </Router>
);

export default Routes;