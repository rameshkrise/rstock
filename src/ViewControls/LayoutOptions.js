import React from 'react';
import {PropTypes} from 'prop-types';

/**
 * Grid/List view options.
 * @param {Object} showListClickHandler - Handles the switching between card/list view.
 */
const LayoutOptions = ({showListClickHandler}) => {
    return <div className='layoutOptions'>
        <ul>
            <a href="# ">
                <li onClick={() => showListClickHandler(false)}>
                        <img src="https://img.icons8.com/windows/26/000000/grid.png" alt="grid view switch"/>
                </li>
            </a>
            <a href="# ">
                <li onClick={() => showListClickHandler(true)}>
                        <img src="https://img.icons8.com/metro/26/000000/menu.png" alt="list view switch"/>
                </li>
            </a>
        </ul>
    </div>;
};

LayoutOptions.propTypes = {
    showListClickHandler:PropTypes.func
}
export default LayoutOptions;