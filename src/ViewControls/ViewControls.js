import React from 'react';
import LayoutOptions from './LayoutOptions';

/**
 * Header strip for holding all the view controls.
 * Currently holds the buttons for Card/List views.
 * @param {Object} props
 */
const ViewControls = (props) => {
    return <div className='viewControlsContainer'>
        <LayoutOptions {...props}/>
    </div>
};

export default ViewControls;