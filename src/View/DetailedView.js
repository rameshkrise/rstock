import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import ImageSlide from './ImageSlide';
import LeftArrow from './LeftArrow';
import RightArrow from './RightArrow';
import AddressDetails from './AddressDetails';
import { withRouter } from 'react-router-dom';

class DetailedView extends Component {
    constructor(props) {
        super(props);
        try{
            this.photos = this.props.location.state.details.resources.photos || [];
        }catch(e) {
            this.photos = [];
        }
        this.state = {
            currentIndex: 0
        }
    }

    showPrevSlide = () => {
        /* This allows cyclic functionalities for left arrow. */
        if(this.state.currentIndex === 0) {
            return this.setState({
                currentIndex: this.photos.length - 1
            });
        }

        this.setState(prevState => ({
            currentIndex: prevState.currentIndex - 1
        }));
    }

    showNextSlide = () => {
        /* This allows cyclic functionalities for right arrow. */
        if(this.state.currentIndex === this.photos.length - 1) {
            return this.setState({
                currentIndex: 0
            });
        }

        this.setState(prevState => ({
            currentIndex: prevState.currentIndex + 1
        }));
    }

    render(){
        const image = this.photos[this.state.currentIndex];
        return ( image ?
            (<div className="detailedViewContainer">
                <AddressDetails address={this.props.location.state.details.address}/>
                <div className="imageContainer">
                    <LeftArrow showPrevSlide={this.showPrevSlide}/>
                    <ImageSlide image={image} />
                    <RightArrow showNextSlide={this.showNextSlide}/>
                </div>
            </div>): <div></div>
        );
    }
}

DetailedView.propTypes = {
    location:PropTypes.object
}

export default withRouter(DetailedView);