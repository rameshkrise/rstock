import React, {Component, Fragment} from 'react';
import ViewControls from '../ViewControls/ViewControls';
import ListView from './ListView';
import GridView from './GridView';
import {customConfigs} from '../Configs/Constants';
import { withRouter } from 'react-router-dom';

/**
 * HomePage is main component which composes the Grid view and List view
 * By default grid page is shown based on the default state showListView=false
 * Component is wrapped with withRouter HOC to provide browser navigation functioalities.
 */
class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showListView: false,
            error: null,
            properties: []
        }
        this.toggleShowList = this.toggleShowList.bind(this);
        this.onItemClickHandler = this.onItemClickHandler.bind(this);
    }

    /**
     * Switch between card/list view based on the clicked icon
     * @param {Boolean} showList - false (Show Card/Grid View), true (Show the List View)
     *
     */
    toggleShowList(showList) {
        this.setState({
            showListView: showList
        });
    }

    /**
     * Fetch request for the properties list
     */
    componentDidMount() {
        fetch(customConfigs.propertiesSrc).then(res => {
            return res.json()})
        .then((result) => {
            this.setState({
                properties: result.properties
            })
        }).catch((error) => {
            this.setState({
                error
            });
        });
    }

    /**Opens the details page to display the address and pics of the clicked item
     * Handles for both card and list views.
     * @param {Object} item
     */
    onItemClickHandler(item) {
        this.props.history.push({
            pathname: "/details",
            state: {details: item}
        });
    }

    /**
     * TODO:: Loading can be a HOC with spinner.
     */
    render() {
        const {showListView, properties} = this.state;
        return (
            <div>
                {properties.length ? (
                    <Fragment>
                        <ViewControls showListClickHandler={this.toggleShowList} />
                        {showListView ? <ListView list={properties} onClickHandler={this.onItemClickHandler}/> :
                        <GridView list={properties} onClickHandler={this.onItemClickHandler}/>}
                    </Fragment>
                    ) : <span>Loading...</span>
                }
            </div>

        );
    }
}

export default withRouter(HomePage);