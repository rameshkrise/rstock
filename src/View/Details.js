import React from 'react';
import {PropTypes} from 'prop-types';
import CardDetail from './CardDetail';
import AddressDetails from './AddressDetails';
import {MathUtil} from '../Util/Util';

/**
 * Details of a property to show on the card.
 * @param {Object} props
 */
const Details = (props) => {
    const {financial, address} = props.item || {};
    const {monthlyRent, listPrice} = financial || {};
    const gross = MathUtil.toFixedPrecision(((monthlyRent * 12) / listPrice)*100, 2);
    return <div className="detailsContainer">
        <CardDetail title="Market Rent" value={`$${monthlyRent}`}/>
        <CardDetail title="Gross Yeild" value={`${gross}%`}/>
        <AddressDetails address={address}/>
    </div>
}

Details.propTypes = {
    financial:PropTypes.object,
    address:PropTypes.object
}

export default Details;