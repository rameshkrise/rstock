import React from 'react';
import {PropTypes} from 'prop-types';

const CardDetail = (props) => {
    const {title, value} = props;
    return (
        <div className="details">
            <h6>{title}</h6>
            <h6 className="values">{value}</h6>
        </div>
    );
}

CardDetail.propTypes = {
    title:PropTypes.string,
    value: PropTypes.string
}
export default CardDetail;