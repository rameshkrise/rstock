import React from 'react';
import {PropTypes} from 'prop-types';

const RightArrow = ({showNextSlide}) => {
  return (
    <button className="rightArrow" onClick={showNextSlide} type="button">
    </button>
  );
}

RightArrow.propTypes = {
    showNextSlide:PropTypes.func
}

export default RightArrow;