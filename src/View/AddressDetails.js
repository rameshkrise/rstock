import React from 'react';
import {PropTypes} from 'prop-types';

/**
 * Address of the property.
 * @param {Object} props
 */
const AddressDetails = ({address}) => {
    const {address1, city, state, zip} = address || {};
    return (
        <div className="address">
            <h6>{address1}</h6>
            <h6>{city}, {state} {zip}</h6>
        </div>
    );
};

AddressDetails.propTypes = {
    address:PropTypes.object.isRequired
}
export default AddressDetails;