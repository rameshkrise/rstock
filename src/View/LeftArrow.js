import React from 'react';
import {PropTypes} from 'prop-types';

const LeftArrow = ({showPrevSlide}) => {
  return (
    <button className="leftArrow" onClick={showPrevSlide} type="button">
    </button>
  );
}

LeftArrow.propTypes = {
    showPrevSlide:PropTypes.func
}

export default LeftArrow;