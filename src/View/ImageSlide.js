import React from 'react';
import {PropTypes} from 'prop-types';

const ImageSlide = ({image}) => {
    return (
        <div className="slide" style={{
            backgroundImage: `url(${image.urlMedium})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 60%'
            }}>
        </div>);
};

ImageSlide.propTypes = {
    image:PropTypes.object
}

export default ImageSlide;
