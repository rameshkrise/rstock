import React from 'react';
import {PropTypes} from 'prop-types';
import Card from './Card';

const GridView = (props) => {
    const {list, onClickHandler} = props;
    return (
        <div className="gridContainer">
            {list && list.map((item, index) => {
                return item.accountId ?<Card item={item} onClickHandler={onClickHandler} key={index}/> : ""})
            }
        </div>
    );
}

GridView.propTypes = {
    list:PropTypes.array,
    onClickHandler: PropTypes.func
}

export default GridView;