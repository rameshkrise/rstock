import React from 'react';
import Details from './Details';
import {PropTypes} from 'prop-types';


/**
 * Card container that shows the image with details.
 * @param {Object} props
 */
const Card = (props) => {
    const {item, onClickHandler} = props || {};
    const {financial, mainImageUrl} = item || {};
    return (
        <div className="cardContainer" onClick={() => onClickHandler(item)}>
            <div className="imageContainer">
                <img src={mainImageUrl} alt='' className="profileIcon"/>
                <div className="overlay">
                    <h6 className="priceValue">
                        {financial && `$${financial.listPrice}`}
                    </h6>
                </div>
            </div>
            <Details item={item}/>
        </div>
    );
}

Card.propTypes = {
    item:PropTypes.object,
    onClickHandler: PropTypes.func
}
export default Card;