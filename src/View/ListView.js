import React, {Component} from 'react';
import {customConfigs} from '../Configs/Constants';
import {MathUtil} from "../Util/Util";
import {PropTypes} from 'prop-types';

/**
 * Presents the properties in table layout.
 */
class ListView extends Component {
    constructor(props) {
        super(props);
        this.headerElements = [{
            className:"imageColumn",
            text: null
        }, {
            className:"addressList",
            text: "Address"
        }, {
            className:"sortable",
            text: "Price"
        }, {
            className:"sortable",
            text: "Current\nRent"
        }, {
            className:"sortable",
            text: "Gross\nYeild"
        }, {
            className:"sortable",
            text: "Year\nBuilt"
        } ];
        this.listPriceDecimalPlaces = customConfigs.listPriceDecimalPlaces;
    }

    render() {
        const {onClickHandler, list} = this.props;
        return (
            <div className="tableResponsive">
                {list.length &&
                    (<table className="table">
                        <thead>
                            <tr>
                                {this.headerElements.map((elem, index) => {
                                    return <th className={elem.className} key={index}>{elem.text}</th>
                                })}
                            </tr>
                        </thead>
                        <tbody className="tableBody">
                            {list.map((item, index) => {
                                const {financial, physical, accountId, address, mainImageUrl} = item;
                                const gross = financial && ((financial.monthlyRent * 12) / financial.listPrice)*100;
                                return (
                                    accountId ? (
                                        <tr key={index} className="propertyList" onClick={() => onClickHandler(item)}>
                                            <td className="photo">
                                                <img src={mainImageUrl} alt='' className="profileIcon"/>
                                            </td>
                                            <td>
                                                {address.address1}
                                                <br/>
                                                {address.city}, {address.state} {address.zip}
                                            </td>
                                            <td>
                                                {financial &&
                                                    `$${MathUtil.toFixedPrecision(financial.listPrice, this.listPriceDecimalPlaces)}`}
                                            </td>
                                            <td>
                                                {financial &&
                                                    `$${MathUtil.toFixedPrecision(financial.monthlyRent, this.monthlyRentDecimalPlaces)}`}
                                            </td>
                                            <td>
                                                {financial && `${MathUtil.toFixedPrecision(gross, 2)}%`}
                                            </td>
                                            <td>
                                                {physical && physical.yearBuilt}
                                            </td>
                                        </tr>
                                    )
                                    :null
                                )
                            })}
                        </tbody>
                    </table>)
                }
            </div>
        );
    }
}

ListView.propTypes = {
    list:PropTypes.array,
    onClickHandler: PropTypes.func
}

export default ListView;
