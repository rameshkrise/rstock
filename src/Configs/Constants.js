export const customConfigs = {
    logoImgUrl: 'https://roofstock-cdn3.azureedge.net/rs-apps/assets/images/roofstock-logo-reversed-907b545688e66dadc22e90bd67d0b66a.svg',
    propertiesSrc: 'http://dev1-sample.azurewebsites.net/properties.json',
    listPriceDecimalPlaces: 2,
    monthlyRentDecimalPlaces: 2
};
