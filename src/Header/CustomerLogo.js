import React from 'react';
//Dependency modules
import {customConfigs} from '../Configs/Constants';

const CustomerLogo = () => {
    return (<div className='headerContainer'>
        <img className='logo' src={customConfigs.logoImgUrl}
        alt='Roofstock main logo'/>
    </div>);
};

export default CustomerLogo;